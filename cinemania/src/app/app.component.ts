import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cinemania';
  tabItems = document.querySelectorAll('.tab-item');
  tabContentItems = document.querySelectorAll('.tab-content-item');
  classList: any;
  id: any;

  // Select tab content item
  selectItem() {
    // Remove all show and border classes
    this.removeBorder();
    this.removeShow();
    // Add border to current tab item
    this.classList.add('tab-border');
    // Grab content item from DOM
    const tabContentItem = document.querySelector(`#${this.id}-content`);
    // Add show class
    tabContentItem.classList.add('show');
  }

  // Remove bottom borders from all tab items
  removeBorder() {
    this.tabItems.forEach(item => {
      item.classList.remove('tab-border');
    });
  }

  // Remove show class from all content items
  removeShow() {
    this.tabContentItems.forEach(item => {
      item.classList.remove('show');
    });
  }
}
